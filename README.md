# AzacCloud Projesi 

Bu proje Openstack kullanmadan daha minimal ve esnek cloud altyapısı yapma amacı ile ortaya çıktı. Küçük ölçekte cloud kullanma amacı ile bu yüzde saniyede binlerce request sayılarını kaldırması amaçlanmamakta deneysel projedir.

### Dökümanlar içerik

 - [Katkı sağla](pages/contribution-tr.md)
 - [Kullanıcı Dökümanları](pages/features_list-tr.md)
 - [Özellikler Listesi](pages/user_docs-tr.md)

## Kullanılan altyapı bileşenleri

Kubernetes ve libvirt altyapı tarafında 2 ana bileşendir. Altyapıda bileşenlerin ayağa kaldırılması API lar aracılığı ile ve sistemlerin üzerinde bulunan sistem servislerine soketler ile haberleşmesi ile olur.

### Temel bişenler 

 - [**Frontend**](https://gitlab.com/azaccloud/frontend) : Kullanıcı Web arayüzü API ile iletişim kurar
 - [**API**](https://gitlab.com/azaccloud/api) : Veritabanı ve RabbitMQ ile iletişim kurar. Kullanıcı doğrulama ve donanım oluşturulacaksa RabbitMQ ya yazma işlerini yapar
 - **RabbitMQ** : Donanım oluşturma istekleri kuyruğa yazılır.
 - [**Consumer**](https://gitlab.com/azaccloud/consumer) : Kuyruktan istekleri okur ve checker servisine donanım oluşturma isteği yollar
 - [**Checker**](https://gitlab.com/azaccloud/checker) : donanım oluşturma isteği ile ilgili istekler burda kontrol  edilir. donanımın yeterli olup olmadığı kullanıldığında ve aralıklı zamanda kontrol edilip veritabanına yazılsada son kontroller yapılır. Buradan sorun yoksa Infrastructure creator'a istek yollanır
 - [**Infrastructure Creator**](https://gitlab.com/azaccloud/infrasreq) : Infrastructure'da istenilen donanımı tahsis etmekle yükümlüdür. Tahsis ettikten sonra check'a işlemin tamam olduğunu bildirir check kontol eder. Sorun yoksa durumu veritabanınan yazar ve kullanıcıya erişim bilgilerini yolar.
 - **PostgreSQL** : Infrastructure ve kullanıcı bilgileri burada tutulur.


#### Infrastructure 

Altyapıda bileşenler libvirt ve kubernetes ve networking ile ilgili bazıl linux sistemi bileşenleridir. Ama kullanıcıya tahsis edilen donanımların ilgili servislerin sistem bileşenlerinin logların metriklerin toplanarak sunulması grafana veya farklı monitöring çözümü ile yapılabilmektedir. Bu kaynaklar kullanıcıya servislerinin loglarını göstermek için ve içerde daha genel bilgiye ulaşmak için farklı monitoring yaklaşımları ile yapılmalıdır.

 - **Kubernetes**
 - **libvirt**
 - **elasticsearch**
   - kullanıcı servislerinin logları burada toplanabilir ve api elasticsearc'e istek yapabilir. Buraya nasıl kaydedilebilir emin değilim. Kubernetes üzeride çalışan controller buraya veri yollayabilir. Libvirt bileşenleri için sistem servisi buraya log yollayabilir.
   - user_token/service_type/service_id şeklinde loglar tutulabilir.
 - [**Infrastructure Creator**](https://gitlab.com/azaccloud/infrasreq)
 
#### API EndPoints

Endpoint'ler AzacCloud'da temel olarak /token endpoint'i ve bu endpoint arkasına gelen cloud bileşenlerini yönetmeyi sağlayan endpoint'ler bulunur. 

 - **/token**
   - **/network**
   - **/storage**
   - **/compute**
   - **/database**
   - **/security**

## System Design

![](pictures/system-architecture.png)

